/** @file modgrav.c Modified gravity functions module
 *
 * Phil Bull, 19.02.2015
 *
 * Modified gravity function definitions and MG utilities.
 *
 * This module defines the modified gravity functions, mutilde(k, a) and 
 * gamma(k, a), which parametrise modifications to the Poisson equation 
 * and slip relation respectively. Both are general functions of scale and 
 * redshift that can be chosen essentially arbitrarily.
 *
 * The modgrav_functions() function returns the MG functions and their 
 * derivatives w.r.t. conformal time.
 *
 */

#include "modgrav.h"

int modgrav_test(int * mg_test_enabled) {
  * mg_test_enabled = 1;
  return _SUCCESS_;
}

/*
 * Return values of modified gravity functions mu and gamma and their 
 * derivatives w.r.t. conformal time at given (tau, k).
 * 
 * @param pba               Input : Pointer to background structure
 * @param pvecback          Input : Pointer to (initialised) vector of 
                                    background quantities
 * @param k                 Input : Wavenumber, k
 * @param tau               Input : Conformal time, tau
 * @param pmg               Output: Struct containing values of MG functions
 * @return the error status
 */
int modgrav_functions(
                 struct background * pba,
                 double * pvecback,
                 double k,
                 double tau,
                 struct modgrav * pmg
                 ) {
  
  // Useful background quantities
  double a, H, aH, aHdot, Hdot, Ez, rho_m, omegaDE, omegaDEdot;
  double p1, p2, q2, t1, t2, M0, kappa2, M2_over_k2;
  double x, y, fz_mu, fz_gam, gk, gkdot_over_gk, cutoff, cutoffdot_over_cutoff;
  double CUTOFF_ZMAX, CUTOFF_WIDTH;
  
  // Set high-z cutoff properties
  CUTOFF_ZMAX = 5.;
  CUTOFF_WIDTH = 5e-3;
  
  // Define useful background quantities
  a = pvecback[pba->index_bg_a];
  H = pvecback[pba->index_bg_H]; // H = H/c, i.e. it's in units of Mpc^-1
  Hdot = pvecback[pba->index_bg_H_prime]; // dH/d(eta)
  aHdot = aH*aH + a*Hdot; // d(aH)/d(eta)
  
  Ez = H / pba->H0;
  aH = a * H;
  
  // Get total matter density in H^2 units, i.e. 'rho_m' = 8piG/3 rho_m(t_i)
  rho_m = pvecback[pba->index_bg_rho_b];
  if (pba->has_cdm == _TRUE_) { rho_m += pvecback[pba->index_bg_rho_cdm]; }
  if (pba->has_dcdm == _TRUE_){ rho_m += pvecback[pba->index_bg_rho_dcdm];}
  
  // Get Omega_DE(a) / Omega_DE(a=1)
  // FIXME: assumes Lambda only for now (i.e. Omega_fld is ignored)
  omegaDE = 0.; omegaDEdot = 0.;
  if (pba->has_lambda == _TRUE_) {
    omegaDE = 1. / Ez / Ez; // = Omega_DE(a) / Omega_DE(a=1)
    omegaDEdot = -2* omegaDE * Hdot / H;
  }
  
  
  // Calculate modified gravity functions (choose which ansatz to use)
  if (pba->mg_ansatz == horndeski) {
    
    // Horndeski-inspired ansatz
    p1 = pba->mg_horndeski_p1; p2 = pba->mg_horndeski_p2;
    t1 = pba->mg_horndeski_t1; t2 = pba->mg_horndeski_t2;
    q2 = pba->mg_horndeski_q2; M0 = pba->mg_horndeski_M;
    M2_over_k2 = M0 * M0 * aH * aH / k / k;
    kappa2 = k / (a * M0 * pba->H0); // Note: H0 includes factor of C
    kappa2 *= kappa2;
    
    // mu and gamma
    pmg->mu    = (1. + q2 * omegaDE * M2_over_k2) 
               / (t1 + t2 * omegaDE * M2_over_k2);
    pmg->gamma = (p1 + p2 * omegaDE * M2_over_k2) 
               / (1. + q2 * omegaDE * M2_over_k2);
    
    // Conformal time derivatives of mu and gamma (need to handle zeros carefully)
    pmg->mu_dot = 0.; pmg->gamma_dot = 0.;
    if (q2 != 0.){
        pmg->mu_dot    +=  1. / ( 1. + kappa2/q2 );
        pmg->gamma_dot += -1. / ( 1. + kappa2/q2 );
    }
    if (p2 != 0.) pmg->gamma_dot +=  1. / (1. + (p1/p2)*kappa2);
    if (t2 != 0.) pmg->mu_dot    += -1. / (1. + (t1/t2)*kappa2);
    pmg->mu_dot *= 2.*aH*pmg->mu; pmg->gamma_dot *= 2.*aH*pmg->gamma;
    
  } else if (pba->mg_ansatz == mgtanh) {
    
    // Ansatz where mu and gamma are given by a tanh function, interpolating 
    // between large and small scales
    
    // Arguments of geometric functions
    p1 = (aH - k) / pba->mg_tanh_Wmu;
    p2 = (aH - k) / pba->mg_tanh_Wgam;
    
    // mu, gamma
    pmg->mu    = 1. + pba->mg_tanh_Bmu  * omegaDE * ( 1. + tanh(p1) );
    pmg->gamma = 1. + pba->mg_tanh_Bgam * omegaDE * ( 1. + tanh(p2) );
    
    // Conformal time derivatives
    pmg->mu_dot    = pba->mg_tanh_Bmu * ( 
                       omegaDEdot * ( 1. + tanh(p1) )
                     + omegaDE * (Hdot / pba->mg_tanh_Wmu / cosh(p1) / cosh(p1))
                     );
    pmg->gamma_dot = pba->mg_tanh_Bgam * ( 
                       omegaDEdot * ( 1. + tanh(p2) )
                     + omegaDE * (Hdot / pba->mg_tanh_Wgam / cosh(p2) / cosh(p2))
                     );
  
  } else if (pba->mg_ansatz == omegade) {
    
    // Ansatz where mu and gamma have a simplified Horndeski scale dependence, 
    // and a redshift evolution proportional to Omega_DE(a). There is also a 
    // cutoff tanh function at high redshift.
    
    // Horndeski-like scale dependent function
    x = pba->mg_lambda * aH / k;
    gk = x*x / (1. + x*x);
    gkdot_over_gk = 2.*aHdot/aH * (1. - gk); // [dg(k,eta)/d(eta)] / g(k,eta)
    
    // High-z cutoff (tanh step function approximation)
    y = (a - 1./(1. + CUTOFF_ZMAX)) / CUTOFF_WIDTH;
    cutoff = 0.5 * ( 1. + tanh(y) );
    cutoffdot_over_cutoff = a * aH * (1. - tanh(y)) / CUTOFF_WIDTH;
    
    // mu, gamma
    pmg->mu    = 1. + pba->mg_mu_lambda  * omegaDE * gk * cutoff;
    pmg->gamma = 1. + pba->mg_gam_lambda * omegaDE * gk * cutoff;
    
    // Conformal time derivatives
    pmg->mu_dot = (pmg->mu - 1.) * 
                  (omegaDEdot/omegaDE + gkdot_over_gk + cutoffdot_over_cutoff);
    
    pmg->gamma_dot = (pmg->gamma - 1.) * 
                  (omegaDEdot/omegaDE + gkdot_over_gk + cutoffdot_over_cutoff);
  
  } else if (pba->mg_ansatz == cpl) {
    
    // Ansatz where mu and gamma have a simplified Horndeski scale dependence, 
    // and a redshift evolution proportional to a CPL-like Taylor expansion. 
    // There is also a cutoff tanh function at high redshift.
    
    // Horndeski-like scale dependent function
    x = pba->mg_lambda * aH / k;
    gk = x*x / (1. + x*x);
    gkdot_over_gk = 2.*aHdot/aH * (1. - gk); // [dg(k,eta)/d(eta)] / g(k,eta)
    
    // High-z cutoff (tanh step function approximation)
    y = (a - 1./(1. + CUTOFF_ZMAX)) / CUTOFF_WIDTH;
    cutoff = 0.5 * ( 1. + tanh(y) );
    cutoffdot_over_cutoff = a * aH * (1. - tanh(y)) / CUTOFF_WIDTH;
    
    // CPL-like redshift dependence
    fz_mu = pba->mg_mu0 + pba->mg_mu1 * (1. - a);
    fz_gam = pba->mg_gam0 + pba->mg_gam1 * (1. - a);
    
    // mu, gamma
    pmg->mu    = 1. + fz_mu * gk * cutoff;
    pmg->gamma = 1. + fz_gam * gk * cutoff;
    
    // Conformal time derivatives (1e-6 in denom. to prevent DIV/0 in GR case)
    pmg->mu_dot = (pmg->mu - 1.) * (-1. * pba->mg_mu1 * a * aH / (fz_mu + 1e-6)
                                    + gkdot_over_gk + cutoffdot_over_cutoff);
    pmg->gamma_dot = (pmg->gamma - 1.) * (-1. * pba->mg_gam1 * a * aH / (fz_gam + 1e-6)
                                    + gkdot_over_gk + cutoffdot_over_cutoff);
    
  } else {
    
    // Simple ansatz where mu, gamma are constant with redshift and scale
    pmg->mu    = pba->mg_A_mu;
    pmg->gamma = pba->mg_A_gamma;
    
    // Conformal time derivatives
    pmg->mu_dot = 0.;
    pmg->gamma_dot = 0.;
    
  } // end ansatz check
  
  // Calculate ULS amplitude factor with MG corrections
  // OmegaM(a) = rho_m(a) / H^2(a)
  // pmg->A_MG = 4.5 * (a*H*a*H) * OmegaM_a * pmg->mu * pmg->gamma;
  pmg->A_MG = 4.5 * rho_m * (a*a) * pmg->mu * pmg->gamma;
  
  // Return success
  return _SUCCESS_;
}


